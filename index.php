<?php

define("PATH_TO", "/services/wiiucanvastest");

if(array_key_exists("export", $_GET)){
	if(strstr($_GET['export'], "/")){
		echo "Sorry, that file path is invalid!";
		return;
	}
	if(!file_exists($_GET['export'])){
		echo "Sorry, this file does not exist!";
		return;
	}
	echo "<img src=\"" . file_get_contents($_GET['export']) . "\"></img>";
	unlink($_GET['export']);
	return;
}

if(array_key_exists("fname", $_POST)){
	if(file_exists($_POST['fname']) or strstr($_POST['fname'], "/")){
		header("HTTP/1.1 401 File Exists");
		echo "The file already exists!";
		return;
	} else {
		file_put_contents($_POST['fname'], $_POST['fdata']);
		header("HTTP/1.1 200 Saved");
		echo "The file was saved successfully! Visit 'http://" . $_SERVER['HTTP_HOST'] . PATH_TO . "?export=" . $_POST['fname'] . "' on a computer to get your image. Please note, by getting your image on the computer, it will be deleted from this service. You will no longer be able to access it on your Wii U!";
		return;
	}
}

if(array_key_exists("fload", $_POST)){
	if(!file_exists($_POST['fload'])){
		header("HTTP/1.1 200 File Not Found");
		echo "The requested file does not exist!";
		return;
	} else {
		header("HTTP/1.1 200 Success");
		echo file_get_contents($_POST['fload']);
		return;
	}
}
?>

<html>

<meta charset="UTF-8"></meta>
<script src="/bootstrap/js/jquery.js"></script>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" media="screen" href="picker/css/colorpicker.css">
	<script type="text/javascript" src="picker/js/colorpicker.js"></script>
	<meta name="viewport" content="width=1280 height=588 user-scalable=no scrollable=no">
	
	<div id="nowiiu" class="cover hidden">
		
		<div id="nowiiumsg" class="messagebox">
			<p class="message-title">Incompatible</p>
			<p class="message-content">This browser is incompatible.<br>This only works with a Wii U browser.<br>Use the back button to navigate away.</p>
		</div>
		
	</div>
	
	<div id="pickercover" class="cover hidden">
		<div class="messagebox twox">
			<p id="picker" class="message-content" style="opacity: 1;"></p>
		</div>
	</div>
	
	<canvas id="canvas" width="100%" height="100%">
	
	</canvas>
	
	<img id="savimg" style="display: none;"></img>
	
	<script>
		var controls = { l_stick_left: 2, l_stick_right: 4, l_stick_up: 6, l_stick_down: 8, r_stick_left: 10, r_stick_right: 12, r_stick_up: 14, r_stick_down: 16, l_stick_press: 26, r_stick_press: 28, a: 32, x: 36, left: 40, right: 42, up: 44, down: 46, zl: 48, zr: 50, plus: 56, minus: 58, r: 54 };
		
		String.prototype.toProperCase = function () {
			return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
		};
		
		var canvas_enabled = true;
		
		var canvas = document.getElementById("canvas");
		var context = document.getElementById("canvas").getContext("2d");
		if(!window.wiiu){
			$("#nowiiu").removeClass("hidden");
			canvas_enabled = false;
		} else {
			canvas.width = document.body.clientWidth;
			canvas.height = document.body.clientHeight;
			context.font="15px Lucida Console";
			context.fillText("Press the right analog stick for help! (Clear the screen to remove this text!)", 10, 50);
			setInterval("update()", 20);
		}
		
		function submitColour(hsb, hex, rgb, el){
			alert("Selected Colour: " + hex);
			context.fillStyle = "#" + hex;
			$('#pickercover').addClass("hidden");
			canvas_enabled = true;
		}
		
		function showPicker(){
			$('#pickercover').removeClass("hidden");
			$('#picker').ColorPicker({onSubmit: function sub(hsb, hex, rgb, el) { submitColour(hsb, hex, rgb, el); } , flat: true, eventName: "touchstart"});
			canvas_enabled = false;
		}
		
		function update(){
			
			var gpState = window.wiiu.gamepad.update();
			if(gpState.tpTouch && canvas_enabled){
				context.fillRect(gpState.contentX, gpState.contentY, 10, 10);
			}
			
			var i;
			var mask = 0x80000000;
			for(i = 0; i < 59; i+= 2, mask = (mask >>> 1)){
				var isHeld = (gpState.hold & 0x7f86fffc & mask) ? 1 : 0;
				if(i == controls.x && canvas_enabled) {
					if(isHeld) clearCanvas();
				}
				if(i == controls.r_stick_press && canvas_enabled){
					if(isHeld) printControls();
				}
				if(i == controls.zl && canvas_enabled){
					if(isHeld) showPicker();
				}
				if(i == controls.zr && canvas_enabled){
					if(isHeld) {
						var r = confirm("Export your image?");
						if(r){
							while(true){
								var filename = prompt("Please specify a file name:");
								if(filename == null){
									var r = confirm("Cancel export?");
									if(r){
										alert("Export canceled!");
										break;
									}
									continue;
								}
								if(filename == "") {
									alert("Please enter a file name!");
								} else {
									var r = confirm("Really save as '" + filename + "'?");
									if(r) {
										$.post("", {fname: filename, fdata: canvas.toDataURL()}, function(data, status) { alert(status.toProperCase() + "!\n\n" + data); });
										break;
									}
								}
							}
						}
					}
				}
				
				if(i == controls.r && isHeld){
					var r = confirm("Load an image?\n(Your current image will be overwritten!)");
					if(r){
						while(true){
							var filename = prompt("Please specify a file name:");
							if(filename == null){
								var r = confirm("Cancel load?");
								if(r) break;
								continue;
							} 
							if(filename == ""){
								alert("Please specify a file name!");
							} else {
								var r = confirm("Really load '" + filename + "'?");
								if(r){
									$.post("", {fload: filename}, function(data, status) { if(data == "The requested file does not exist!") alert(data); if(status == "success" && clearCanvas()) { var img = document.getElementById("savimg"); img.src=data; alert("Loaded!"); context.drawImage(img, 0, 0);  } });
									break;
								}
							}
							
						}
					}
				}
				
			}
			
		}
		
		function clearCanvas(){
			var r = confirm("Really delete your drawing?");
			if(r){
				alert("Your drawing was deleted!");
			} else {
				alert("Your drawing was not deleted!");
				return false;
			}
			var canvas = document.getElementById("canvas");
			context.clearRect(0, 0, canvas.width, canvas.height);
			return true;
		}
		
		function printControls(){
			alert("Controls:\nX: Clear\nZL: Colour Picker\nZR: Export Image\nR: Import Image");
		}
		
	</script>

</html>